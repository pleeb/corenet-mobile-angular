import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";

export interface IItem {
  CustomerID: number;
  FirstName: string;
  LastName: string;
  MemberType: string;
  Company: string;
  Title: string;
  Email: string;
  Phone: string;
  Mobile: string;
}
@Component({
  selector: "app-item",
  template: `
    <ion-item lines="full">
      <p style="line-height:1.5;color:#666;font-size:14px;">
        <strong>{{ name }}</strong>
        <br />
        {{ position }}
        <br />
        {{ company }}
        <br />
        {{ telephone }}
        <br />
        {{ email }}
        <br />{{ type }}
      </p>
    </ion-item>
  `,
  styleUrls: ["./directory.page.scss"]
})
export class ItemComponent implements OnInit {
  @Input() item: IItem;
  name: string;
  position: string;
  company: string;
  telephone: string;
  email: string;
  type: string;
  constructor() {}
  ngOnInit() {
    console.log("ngOnInit item component", this.item);
    this.name = `${this.item.FirstName} ${this.item.LastName}`;
    this.position = this.item.Title;
    this.company = this.item.Company;
    this.telephone = this.item.Phone;
    this.email = this.item.Email;
    this.type = this.item.MemberType;
  }
}
