import { Component, OnInit } from "@angular/core";
import { IItem } from "./item.component";
import { AppService } from "../app.service";

@Component({
  selector: "app-directory",
  templateUrl: "./directory.page.html",
  styleUrls: ["./directory.page.scss"]
})
export class DirectoryPage implements OnInit {
  items: IItem[] = [];
  constructor(private appService: AppService) {}
  ngOnInit() {
    this.getUsers();
  }
  async getUsers() {
    this.items = await this.appService.getUsers();
  }
}
