import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { DirectoryPage } from "./directory.page";
import { HeaderModule } from "../component/header/header.module";
import { ItemComponent } from "./item.component";

const routes: Routes = [
  {
    path: "",
    component: DirectoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
    RouterModule.forChild(routes)
  ],
  declarations: [DirectoryPage, ItemComponent]
})
export class DirectoryPageModule {}
