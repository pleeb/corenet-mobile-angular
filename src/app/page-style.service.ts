import { Injectable } from "@angular/core";
import randomcolor from "randomcolor";
import { committees } from "./data";
import { AppService } from "./app.service";

@Injectable({
  providedIn: "root"
})
export class PageStyleService {
  private get tab() {
    return this.url.split("/").pop();
  }
  private get url() {
    return this.appService.url;
  }
  private readonly defaultColor: string = "#3880ff";
  constructor(private appService: AppService) {}
  getHeaderColor(event?: { EventID?: string | number; committee?: string }) {
    if (event) {
      return this.getCommitteeColor(event);
    }
    const EventID = this.getEventId();
    event = this.appService._getEvent({ EventID });
    return event ? this.getCommitteeColor(event) : this.defaultColor;
  }
  getHeaderTitle() {
    if (this.tab === "calendar" || this.tab.startsWith("event?"))
      return "upcoming events";
    return this.tab;
  }
  getEventId() {
    const url = this.url;
    return +url.split("?EventID=").pop();
  }
  getHeaderImage() {
    const pages = [
      "about",
      "calendar",
      "directory",
      "help",
      "preferences",
      "resources",
      "event_1",
      "event_2"
    ];
    if (pages.includes(this.tab))
      return `http://blasts.bermangrp.com/corenet-mobile-app/src/assets/image/header/${
        this.tab
      }.jpg`;
    else {
      try {
        const EventID = this.getEventId();
        const event = this.appService._getEvent({ EventID });
        const image = event.image;
        return image;
      } catch (err) {
        const image = `http://blasts.bermangrp.com/corenet-mobile-app/src/assets/image/header/${
          pages[Math.floor(Math.random() * pages.length)]
        }.jpg`;
        console.log(this.url, "random header image!", image);
        return image;
      }
    }
  }
  private seedRandomColor(seed: number | string): string {
    if (isNaN(seed as number)) {
      console.log(
        "bad event color seed at",
        this.url,
        "returning default color"
      );
      return this.defaultColor;
    }
    const options = {
      seed,
      format: "hex"
    };
    const color = randomcolor(options);
    console.log(
      "seeded random color at",
      this.url,
      seed,
      "returning color",
      color
    );
    return color;
  }
  private getCommitteeColor(event: any): string {
    const arr = Object.keys(committees).map(key => committees[key]);
    const committee = arr.find(e => {
      const result = normStr(event.committee || "nocommitteee").includes(
        normStr(e.title)
      );
      return result;
    });

    return committee
      ? committee.color
      : this.seedRandomColor(event.EventID || 123);
  }
}
function normStr(value: string): string {
  // strip all special char, spaces, to lower case
  return value ? value.replace(/[^a-zA-Z]/g, "").toLowerCase() : "";
}
