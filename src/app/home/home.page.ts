import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"]
})
export class HomePage implements OnInit {
  constructor(private appService: AppService) {}
  redirectTo(tab: string) {
    this.appService.redirectToURL(tab);
  }
  ngOnInit() {
    let elements = document.querySelectorAll(".tabbar");

    // if (elements != null) {
    //   Object.keys(elements).map(key => {
    //     elements[key].style.display = "none";
    //   });
    // }
  }
}
