import { Component, OnInit } from "@angular/core";

interface IItem {
  title: string;
  url: string;
  icon: string;
}
@Component({
  selector: "app-resources",
  templateUrl: "./resources.page.html",
  styleUrls: ["./resources.page.scss"]
})
export class ResourcesPage implements OnInit {
  items: IItem[] = [
    {
      title: "Podcasts",
      icon: "radio",
      url: "https://newyorkcity.corenetglobal.org/news/podcasts"
    },
    {
      title: "Blog Posts",
      icon: "browsers",
      url: "https://newyorkcity.corenetglobal.org/news/blog"
    },
    {
      title: "Newsletter",
      icon: "paper",
      url: "https://newyorkcity.corenetglobal.org/news/newsletter"
    },
    {
      title: "Knowledge Center",
      icon: "bulb",
      url: "https://corenetglobal.org/KCO/landing.aspx?navItemNumber=20779"
    }
  ];
  constructor() {}
  ngOnInit() {}
}
