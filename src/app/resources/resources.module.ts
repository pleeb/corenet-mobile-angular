import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { ResourcesPage } from "./resources.page";
import { ItemComponent } from "./item/item.component";
import { HeaderModule } from "../component/header/header.module";

const routes: Routes = [
  {
    path: "",
    component: ResourcesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HeaderModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ResourcesPage, ItemComponent]
})
export class ResourcesPageModule {}
