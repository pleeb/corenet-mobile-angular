import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.scss"]
})
export class ItemComponent implements OnInit {
  @Input() icon: string;
  @Input() title: string;
  @Input() url: string;
  constructor() {}
  ngOnInit() {}
  openLink() {
    window.open(this.url, "_system", "location=yes");
    return false;
  }
}
