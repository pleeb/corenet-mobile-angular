import { Injectable } from "@angular/core";
import { CanActivate, CanActivateChild } from "@angular/router";
import { AppService } from "./app.service";

@Injectable({
  providedIn: "root"
})
export class AuthGuardService implements CanActivate, CanActivateChild {
  constructor(private appService: AppService) {}
  canActivate() {
    this.appService.getLoggedUser().then(user => {
      console.log("auth login", !!user);
      !user && this.appService.logoutUser();
    });
    return true;
  }
  canActivateChild() {
    return this.canActivate();
  }
}
