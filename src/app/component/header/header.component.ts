import { Component, OnInit, Input } from "@angular/core";
import { PageStyleService } from "../../page-style.service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  readonly logoWidth: number = 50;
  readonly headerColor: string;
  readonly bannerWidth: number = 100;
  readonly headerImage: string;
  readonly headerTitle: string;
  constructor(private style: PageStyleService) {
    this.headerImage = style.getHeaderImage();
    this.headerColor = style.getHeaderColor();
    this.headerTitle = style.getHeaderTitle();
    console.log(this.headerColor, this.headerImage);
  }
  ngOnInit() {}
}
