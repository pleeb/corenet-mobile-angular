import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IonicModule } from "@ionic/angular";
import { HeaderComponent } from "./header.component";
import { LogoComponent } from "../logo/logo.component";
import { BannerComponent } from "../banner/banner.component";
import { SettingsFabComponent } from "../settings-fab/settings-fab.component";

@NgModule({
  declarations: [
    HeaderComponent,
    LogoComponent,
    BannerComponent,
    SettingsFabComponent
  ],
  imports: [CommonModule, IonicModule],
  exports: [
    HeaderComponent,
    LogoComponent,
    BannerComponent,
    SettingsFabComponent
  ]
})
export class HeaderModule {}
