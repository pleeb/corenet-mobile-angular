import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsFabComponent } from './settings-fab.component';

describe('SettingsFabComponent', () => {
  let component: SettingsFabComponent;
  let fixture: ComponentFixture<SettingsFabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsFabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsFabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
