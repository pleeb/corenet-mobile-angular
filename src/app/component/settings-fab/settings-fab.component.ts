import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-settings-fab",
  templateUrl: "./settings-fab.component.html",
  styleUrls: ["./settings-fab.component.scss"]
})
export class SettingsFabComponent implements OnInit {
  get display(): boolean {
    const t = this.router.url.split("/").pop() !== "settings";
    console.log(this.constructor.name, "get display()", t);
    return t;
  }
  constructor(private router: Router) {
    console.log(this.constructor.name, "constructor");
  }
  routePage() {
    this.router.navigateByUrl("/tabs/settings");
  }
  ngOnInit() {
    console.log(this.constructor.name, "ngOnInit");
  }
}
