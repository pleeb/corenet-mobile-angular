import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-banner",
  templateUrl: "./banner.component.html",
  styleUrls: ["./banner.component.scss"]
})
export class BannerComponent implements OnInit {
  @Input() resource: string;
  @Input() width: number | string;
  @Input() title: string;
  constructor() {}
  ngOnInit() {}
}
