import { map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import {
  Http,
  Headers,
  RequestOptions,
  Request,
  Response,
  URLSearchParams
} from "@angular/http";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";

export interface IAsyncApiMethods {
  apiLoginUser(data: ICredentials): Promise<IUser>;
  apiGetUsers(params): Promise<any>;
  apiGetEvents(params): Promise<any>;
}
export interface IEvent {
  EventID: string;
}
export interface ICredentials {
  username: string;
  password: string;
}
export interface IUser {
  username: string;
  password?: string;
}
const base = "https://aptifyweb.corenetglobal.org/AptifyServicesAPI/services";
type Method = "GET" | "POST" | "DELETE" | "PUT";
@Injectable({
  providedIn: "root"
})
export class ApiService implements IAsyncApiMethods {
  private requestResponse: any;
  constructor(
    private http: Http,
    private router: Router,
    private storage: Storage
  ) {
    // this.test();
  }
  async test() {
    // https://aptifyweb.corenetglobal.org/AptifyServicesAPI/services/Authentication/Login/Web?UserName=NYCchapter&Password=CorenetNYC
    return (
      this.http
        // .get(
        //   "/AptifyServicesAPI/services/Authentication/Login/Web?UserName=NYCchapter&Password=CorenetNYC"
        // )
        .get("/api?search=hello")
        .toPromise()
        .then(console.log)
        .catch(e => console.log("WARNING!!!", e))
    );
  }
  async apiLoginUser({ username, password }: ICredentials): Promise<any> {
    const path = `/Authentication/Login/Web?username=NYCchapter&password=CorenetNYC`;
    return this.proxyRequest("GET", "/api/endpoint");
    return;
    return this.request("GET", path)
      .toPromise()
      .then(r => r.toJson())
      .catch(console.log.bind(null, "ERROR"));
  }
  async apiGetEvents(params): Promise<any> {
    const path = `/Authentication/Login/Web?username={username}&password={password}`;
    return;
  }
  async apiGetUsers(params): Promise<any> {
    const path = `/Authentication/Login/Web?username={username}&password={password}`;
    return;
  }
  private proxyRequest(method: Method, path: string, params?: any) {
    const url = "localhost:3000" + path;
    const headers = new Headers();
    const options = {
      method,
      headers,
      url,
      search: null,
      body: null,
      withCredentials: true
    };
    switch (method) {
      case "GET":
        if (params) {
          const search = new URLSearchParams();
          for (const key in params) {
            search.append(key, params[key]);
          }
          options.search = search;
        }
        break;
      case "PUT":
        headers.append("Content-Type", "application/json");
        headers.append(
          "Authorization",
          "Bearer d440f7ff-b578-4654-9be8-c4837008e987"
        );
        options.body = JSON.stringify(params);
        break;
      default:
        // code...
        break;
    }
    const req = new Request(new RequestOptions(options));
    return this.http.request(req).pipe(map((res: Response) => res.json()));
  }
  private request(method: Method, path: string, params?: any) {
    const url = base + path;
    const headers = new Headers();
    const options = {
      method,
      headers,
      url,
      search: null,
      body: null,
      withCredentials: true
    };
    switch (method) {
      case "GET":
        if (params) {
          const search = new URLSearchParams();
          for (const key in params) {
            search.append(key, params[key]);
          }
          options.search = search;
        }
        break;
      case "PUT":
        headers.append("Content-Type", "application/json");
        headers.append(
          "Authorization",
          "Bearer d440f7ff-b578-4654-9be8-c4837008e987"
        );
        options.body = JSON.stringify(params);
        break;
      default:
        // code...
        break;
    }
    const req = new Request(new RequestOptions(options));
    return this.http.request(req).pipe(map((res: Response) => res.json()));
  }
}
