import { Component, OnInit } from "@angular/core";

interface IItem {
  title: string;
  page: string;
  icon: string;
}
@Component({
  selector: "app-settings",
  templateUrl: "./settings.page.html",
  styleUrls: ["./settings.page.scss"]
})
export class SettingsPage implements OnInit {
  items: IItem[] = [
    // {
    //   title: "Preferences",
    //   page: "preferences",
    //   icon: "arrow-dropright-circle"
    // },
    { title: "Help", page: "help", icon: "open" }
    // { title: "Logout", page: "logout", icon: "log-out" }
  ];
  constructor() {}
  ngOnInit() {}
}
