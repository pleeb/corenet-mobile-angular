import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule, RouterLink } from "@angular/router";

import { IonicModule } from "@ionic/angular";
import { HeaderModule } from "../component/header/header.module";
import { SettingsPage } from "./settings.page";

import { ItemComponent } from "./item/item.component";

const routes: Routes = [
  {
    path: "",
    component: SettingsPage
  },
  // {
  //   path: "preferences",
  //   loadChildren: "./preferences/preferences.module#PreferencesPageModule"
  // },
  { path: "help", loadChildren: "./help/help.module#HelpPageModule" }
  // { path: "logout", loadChildren: "./logout/logout.module#LogoutPageModule" }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    HeaderModule
  ],
  declarations: [SettingsPage, ItemComponent]
})
export class SettingsPageModule {}
