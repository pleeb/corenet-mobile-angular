import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.scss"]
})
export class ItemComponent implements OnInit {
  @Input() title: string;
  @Input() icon: string;
  @Input() page: string;
  constructor(private router: Router) {}
  ngOnInit() {}
  routePage() {
    console.log(
      this.router.getCurrentNavigation(),
      this.router.routerState,
      this.router
    );
    this.router.navigateByUrl(`/tabs/settings/${this.page}`);
  }
}
