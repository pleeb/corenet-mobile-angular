import { Component, OnInit } from "@angular/core";
import { AppService } from "../../app.service";
@Component({
  selector: "app-logout",
  templateUrl: "./logout.page.html",
  styleUrls: ["./logout.page.scss"]
})
export class LogoutPage implements OnInit {
  constructor(private appService: AppService) {
    console.log("logging out");
    appService.logoutUser();
  }

  ngOnInit() {}
}
