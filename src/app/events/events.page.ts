import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

export interface IItem {
  ID: number;
  EventID: number;
  MeetingTitle: string;
  committee?: string;
  title?: string;
  StartDate: Date | string;
  EndDate: Date | string;
  Status: string;
  AvailSpace: number;
  TotalRegistrants: number;
  TotalWaitList: number;
  DiscountCode: null;
  AddressLine1: string;
  City: string;
  State: string;
  Country: string;
  MobileTitle: string;
  MobileDescription: string;
}
export enum Status {
  NotRegistered = "Register",
  Registered = "Registered"
}
@Component({
  selector: "app-events",
  templateUrl: "./events.page.html",
  styleUrls: ["./events.page.scss"]
})
export class EventsPage implements OnInit {
  items: IItem[] = [];
  constructor(private appService: AppService) {}
  async ngOnInit() {
    this.items = await this.appService.getEvents();
  }
}
