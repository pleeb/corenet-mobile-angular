import { Component, OnInit } from "@angular/core";
import { AppService } from "../../app.service";
import { PageStyleService } from "../../page-style.service";
import { IItem, Status } from "../events.page";

@Component({
  selector: "app-event",
  templateUrl: "./event.page.html",
  styleUrls: ["./event.page.scss"]
})
export class EventPage implements OnInit {
  item: IItem;
  address: string[];
  date: Date;
  status: Status;
  color: string;
  constructor(private appService: AppService, private style: PageStyleService) {
    this.getEvent();
  }
  ngOnInit() {}
  async getEvent() {
    const url = this.appService.url;
    const EventID: number = +url.split("?EventID=").pop();
    if (isNaN(EventID)) throw "bad event identifier at " + url;
    const event = await this.appService.getEvent({ EventID });
    this.item = event;
    this.address = [event.AddressLine1, `${event.City}, ${event.State}`];
    this.date = new Date(event.StartDate);
    this.color = this.style.getHeaderColor(event);
  }
  externalRedirect() {
    const base = "https://resources.corenetglobal.org/Meetings/Meeting.aspx";
    const url = `${base}?ID=${this.item.EventID}`;
    window.open(url, "_system", "location=yes");
  }
}
