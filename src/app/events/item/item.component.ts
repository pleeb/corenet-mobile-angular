import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { IItem, Status } from "../events.page";
import { PageStyleService } from "../../page-style.service";
export { IItem, Status } from "../events.page";

@Component({
  selector: "app-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.scss"]
})
export class ItemComponent implements OnInit {
  @Input() item: IItem;
  constructor(private router: Router, private style: PageStyleService) {}
  ngOnInit() {}
  routePage() {
    this.router.navigateByUrl(
      `/tabs/calendar/event?EventID=${this.item.EventID}`
    );
  }
}
