import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";
import { IItem, Status } from "./item.component";

@Component({
  selector: "app-item-button",
  template: `
    <a [ngClass]="'register-button ' + icon" (click)="routePage()">
      <ion-icon [name]="'md-' + icon" padding-bottom></ion-icon>
      <span>{{ status }}</span>
    </a>
  `,
  styleUrls: ["./item.component.scss"]
})
export class ButtonComponent implements OnInit {
  @Input() status: Status;
  @Input() item: IItem;
  get isRegistered() {
    return this.status === Status.Registered;
  }
  get icon() {
    return this.isRegistered ? "checkmark" : "add";
  }
  constructor(private router: Router) {}
  ngOnInit() {}
  routePage() {
    this.router.navigateByUrl(
      `/tabs/calendar/event?EventID=${this.item.EventID}`
    );
  }
}
