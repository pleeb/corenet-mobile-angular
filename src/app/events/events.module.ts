import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { EventsPage } from "./events.page";
import { HeaderModule } from "../component/header/header.module";
import { ItemComponent } from "./item/item.component";
import { ButtonComponent } from "./item/button.component";

const routes: Routes = [
  {
    path: "",
    component: EventsPage
  },
  { path: "event", loadChildren: "./event/event.module#EventPageModule" }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    HeaderModule
  ],
  declarations: [EventsPage, ItemComponent, ButtonComponent]
})
export class EventsPageModule {}
