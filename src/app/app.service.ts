import { map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import {
  Http,
  Headers,
  RequestOptions,
  Request,
  Response,
  URLSearchParams
} from "@angular/http";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { ApiService } from "./api.service";
import { data as mocks } from "./data";

export interface ICredentials {
  username: string;
  password: string;
}
interface IApiConfig {
  "api/events": string;
  events: string;
}
@Injectable({
  providedIn: "root"
})
export class AppService {
  private requestResponse: any;
  get tab() {
    return this.url.split("/").pop();
  }
  get url() {
    return this.router.url;
  }
  constructor(
    private http: Http,
    private router: Router,
    private storage: Storage,
    private apiService: ApiService
  ) {}
  private async storeLoggedUser(data) {
    this.storage.set("user", data);
  }
  private formatEvents(events) {
    const today = new Date();
    return events
      .map(e => {
        const meetingTitle = e.MeetingTitle;
        const [committee, title] = meetingTitle.split(":");
        return {
          ...e,
          committee,
          title,
          StartDate: new Date(e.StartDate),
          EndDate: new Date(e.EndDate)
        };
      })
      .filter(e => +e.StartDate >= +today);
  }
  async getLoggedUser() {
    const u = await this.storage.get("user");
    console.log("get logged user", u);
    return u;
  }
  async loginUser({ username, password } = {} as any) {
    // mock
    // UserName=NYCchapter&Password=CorenetNYC
    // https://aptifyweb.corenetglobal.org/AptifyServicesAPI/services/Authentication/Login/Web?UserName=NYCchapter&Password=CorenetNYC
    const user = {
      UserId: 90041,
      UserName: "NYCchapter",
      Email: "spadgett@corenetglobal.org",
      FirstName: "NYC",
      LastName: "Chapter",
      Title: "Used for WebService Account DO NOT ALTER THIS RECO",
      LinkId: "50737",
      CompanyId: "",
      TokenId: "a18ac317-3aeb-4966-ad23-68f751d5d302",
      Server: "CorenetSQL-02",
      Database: "Aptify",
      AptifyUserID: 58,
      AptifyUser: "CNG\\ebizuser",
      AuthenticationTime: "2019-01-31T10:20:28.1143338-05:00",
      AuthenticatedPersonId: 50737
    };
    // await this.storeLoggedUser(user);
    const u = await this.apiService
      .apiLoginUser({
        username: "NYCchapter",
        password: "CorenetNYC"
      })
      .catch(e => console.error("caught error", e));
    console.log(u);
    this.redirectToURL("/tabs/home");
    return user;
  }
  async logoutUser() {
    await this.storage.clear();
    this.redirectToURL("/login");
  }
  async apiGetEvent(EventID: number) {
    return null;
  }
  async apiGetEvents() {
    return null;
  }
  async getEvents() {
    let events;
    try {
      const config: IApiConfig = await this.http
        .get("http://blasts.bermangrp.com/corenet-mobile-app/config.json")
        .toPromise()
        .then(e => e.json());
      const { "api/events": eventsApi, events: eventsResource } = config;
      const _events = await this.http
        .get(eventsApi)
        .toPromise()
        .then(e => e.json())
        .catch(e => {
          throw e;
        });
      events = this.formatEvents(_events);
    } catch (err) {
      console.log("WARNING caught get events error", err);
      events = this.formatEvents(mocks.results);
    }
    await this.storage.set("events", events || []);
    this._storage.set("events", events || []);
    return events;
  }
  private _storage: Map<string, any> = new Map();
  async getEvent({ EventID }: { EventID: number }) {
    const events: any[] = await this.storage.get("events");
    const event = events.find(e => +e.EventID === +EventID);
    return event;
  }
  _getEvent({ EventID }: { EventID: number }) {
    const events: any[] = this._storage.get("events") || [];
    const event = events.find(e => +e.EventID === +EventID);
    return event;
  }
  async getUsers(params?) {
    const users = [
      {
        EventID: 5477,
        EventName: "Workshop Around the World in 90 Minutes",
        StartDate: "9/30/2015 12:00:00 AM",
        DateRegistered: "9/15/2015 12:00:00 AM",
        StatusID: 1,
        AttendeeStatus: "Registered",
        CustomerID: 22586,
        FirstName: "Lisa",
        LastName: "Anselmo",
        MemberType: "Member Service Provider",
        PrimaryChapter: "New York City",
        Company: "IA Interior Architects",
        Title: "Director of Client Services",
        Email: "l.anselmo@interiorarchitects.com",
        AddressLine1: "100 Broadway",
        AddressLine2: "Suite 800",
        City: "New York",
        State: "NY",
        Zipcode: "10005",
        Country: "United States",
        Phone: "1.212.672-0297",
        Mobile: ".."
      }
    ];
    await this.storage.set("users", users);
    return users;
  }
  async getUser(params?) {
    if (!params) return this.getLoggedUser();
    const user = {
      EventID: 5477,
      EventName: "Workshop Around the World in 90 Minutes",
      StartDate: "9/30/2015 12:00:00 AM",
      DateRegistered: "9/15/2015 12:00:00 AM",
      StatusID: 1,
      AttendeeStatus: "Registered",
      CustomerID: 22586,
      FirstName: "Lisa",
      LastName: "Anselmo",
      MemberType: "Member Service Provider",
      PrimaryChapter: "New York City",
      Company: "IA Interior Architects",
      Title: "Director of Client Services",
      Email: "l.anselmo@interiorarchitects.com",
      AddressLine1: "100 Broadway",
      AddressLine2: "Suite 800",
      City: "New York",
      State: "NY",
      Zipcode: "10005",
      Country: "United States",
      Phone: "1.212.672-0297",
      Mobile: ".."
    };
    return user;
  }
  redirectToURL(path) {
    this.router.navigateByUrl(path);
  }
}
