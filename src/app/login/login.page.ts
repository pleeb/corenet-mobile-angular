import * as rx from "rxjs";
import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"]
})
export class LoginPage implements OnInit {
  username: string;
  password: string;
  fetching: boolean = false;
  constructor(private appService: AppService) {
    // this.display = true;
  }
  ngOnInit() {}
  async doLogin() {
    const { username, password } = this;
    console.log("do login", username, password);
    this.fetching = true;
    await rx.timer(500 + Math.random() * 1000).toPromise();
    await this.appService.loginUser({ username, password });
    this.fetching = false;
    // this.display = false;
  }
}
