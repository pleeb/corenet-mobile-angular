import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { TabsPage } from "./tabs.page";
import { AuthGuardService } from "../auth-guard.service";

const routes: Routes = [
  {
    path: "tabs",
    component: TabsPage,
    children: [
      {
        path: "settings",
        loadChildren: "../settings/settings.module#SettingsPageModule"
      },
      { path: "home", loadChildren: "../home/home.module#HomePageModule" },
      {
        path: "resources",
        loadChildren: "../resources/resources.module#ResourcesPageModule"
      },
      {
        path: "directory",
        loadChildren: "../directory/directory.module#DirectoryPageModule"
      },
      {
        path: "calendar",
        loadChildren: "../events/events.module#EventsPageModule"
      },
      { path: "about", loadChildren: "../about/about.module#AboutPageModule" },
      {
        path: "",
        redirectTo: "/home",
        pathMatch: "full"
      }
    ]
  },
  {
    path: "",
    redirectTo: "/tabs/home",
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
