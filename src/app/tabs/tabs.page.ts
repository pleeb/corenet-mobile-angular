import { Component, ViewChild } from "@angular/core";
import { NavController, Events, IonTabs } from "@ionic/angular";
import { AppService } from "../app.service";

@Component({
  selector: "app-tabs",
  templateUrl: "tabs.page.html",
  styleUrls: ["tabs.page.scss"]
})
export class TabsPage {
  @ViewChild("myTabs") tabRef: IonTabs;
  visible = "visible";
  myFunction() {
    console.log("test!!!");
  }
  selectedIndex: number = 0;
  tabHomeClickEvent: any; //for home tab click event
  constructor(
    private appService: AppService,
    public navCtrl: NavController,
    public events: Events
  ) {
    let that = this;
    this.tabHomeClickEvent = function(e) {
      if (0 == that.selectedIndex) {
        //tab home be clicked!
        //do some thing
      }
    };
  }
  private el: any;
  ionTabsDidChange() {
    console.log("tabs did change");
  }
  hideTab(value: boolean) {
    this.el.hidden = value;
  }
  ionViewDidEnter() {
    this.el = (this.tabRef.tabBar as any).el;
    if (this.appService.url === "/tabs/home") this.hideTab(true);
    this.tabRef.ionTabsDidChange.subscribe(e => this.hideTab(e.tab === "home"));
    console.log("ion view did enter", (this.tabRef.tabBar as any).el);
  }

  // <ion-tabs (ionChange)="tabChange($event)">
  tabChange(tab) {
    this.selectedIndex = tab.index;
    console.log(this.selectedIndex);
    //index equals 0/other to add/remove tab home click event
    if (0 == this.selectedIndex) {
      this.tabHomeAddClickEvent();
    } else {
      this.tabHomeRemoveClickEvent();
    }
  }

  //ion-md-home: <ion-tab home [root]="tabHome" tabTitle="Home" tabIcon="md-home"></ion-tab>
  private tabHomeAddClickEvent(): void {
    let that = this;
    let tabHomeIcon = document.getElementsByClassName("ion-md-home");
    if (tabHomeIcon.length == 1) {
      tabHomeIcon[0].addEventListener("click", that.tabHomeClickEvent, true);
    }
  }

  private tabHomeRemoveClickEvent(): void {
    let that = this;
    let tabHomeIcon = document.getElementsByClassName("ion-md-home");
    if (tabHomeIcon.length == 1) {
      tabHomeIcon[0].removeEventListener("click", that.tabHomeClickEvent, true);
    }
  }
}
