import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuardService } from "./auth-guard.service";

const routes: Routes = [
  {
    path: "",
    loadChildren: "./tabs/tabs.module#TabsPageModule"
  },
  // {
  //   path: "login",
  //   loadChildren: "./login/login.module#LoginPageModule",
  //   canActivate: [AuthGuardService]
  // },
  { path: "settings", redirectTo: "./tabs/settings" },
  // { path: "preferences", redirectTo: "./tabs/settings/preferences" },
  { path: "help", redirectTo: "./tabs/settings/help" }
  // { path: "logout", redirectTo: "./tabs/settings/logout" }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
